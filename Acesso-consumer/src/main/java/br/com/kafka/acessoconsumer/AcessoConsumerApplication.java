package br.com.kafka.acessoconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class AcessoConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AcessoConsumerApplication.class, args);
	}

}
