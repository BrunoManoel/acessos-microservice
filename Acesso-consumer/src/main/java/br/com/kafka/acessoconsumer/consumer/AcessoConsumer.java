package br.com.kafka.acessoconsumer.consumer;

import br.com.kafka.acessoconsumer.consumer.model.Acesso;
import br.com.mastertech.acesso.acessos.DTOs.AcessoDTO;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.Arrays;
import java.util.List;

@Component
public class AcessoConsumer {

    @KafkaListener(topics = "spec4-mateus-affonso", groupId = "teste")
    public void receber(@Payload AcessoDTO acesso) {
        System.out.println("O cliente com id: " + acesso.getClienteId()+ " acessou a porta de id:  " + acesso.getPortaId());
        try{
            escreveCsv(acesso);
        } catch (IOException e) {
            e.getMessage();
        }
    }

    private void escreveCsv(AcessoDTO acesso) throws IOException {

        String pathToCsv = "/home/a2/Documentos/especializacao 4/proposta case acesso/acessos.csv";
        File csvFile = new File(pathToCsv);
        String row = "";

        if (csvFile.isFile()) {
            BufferedReader csvReader = new BufferedReader(new FileReader(pathToCsv));
            while ((row = csvReader.readLine()) != null) {
                String[] data = row.split(",");
                // do something with the data
            }
            insereLinhaCsv(acesso, pathToCsv);
            csvReader.close();
        } else {
            insereLinhaCsv(acesso, pathToCsv);
        }
    }

    private void insereLinhaCsv(AcessoDTO acesso, String pathToCsv) throws IOException {
        List<String> rowData = Arrays.asList(
                "Cliente",
                Integer.toString(acesso.getClienteId()),
                "Porta",
                Integer.toString(acesso.getPortaId())
        );

        FileWriter csvWriter = new FileWriter(pathToCsv);
        csvWriter.append(String.join(",", rowData));
        csvWriter.append("\n");
        csvWriter.flush();
        csvWriter.close();
    }
}
