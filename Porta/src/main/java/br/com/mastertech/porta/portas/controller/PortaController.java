package br.com.mastertech.porta.portas.controller;

import br.com.mastertech.porta.portas.DTOs.InserePortaDTO;
import br.com.mastertech.porta.portas.models.Porta;
import br.com.mastertech.porta.portas.services.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    PortaService portaService;

    @GetMapping("/{id}")
    public Porta findPortaById(@PathVariable (name = "id") int id){
        return portaService.findById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Porta criaPorta(@RequestBody InserePortaDTO inserePortaDTO){
        return  portaService.criaPorta(inserePortaDTO);
    }

}
