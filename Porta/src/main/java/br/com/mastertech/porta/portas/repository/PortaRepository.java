package br.com.mastertech.porta.portas.repository;

import br.com.mastertech.porta.portas.models.Porta;
import org.springframework.data.repository.CrudRepository;

public interface PortaRepository extends CrudRepository<Porta, Integer> {
}
