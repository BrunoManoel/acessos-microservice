package br.com.mastertech.porta.portas.services;
import br.com.mastertech.porta.portas.DTOs.InserePortaDTO;
import br.com.mastertech.porta.portas.exception.PortaNotFoundException;
import br.com.mastertech.porta.portas.models.Porta;
import br.com.mastertech.porta.portas.repository.PortaRepository;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    PortaRepository portaRepository;

    public Porta findById(int id) {
        Optional<Porta> portaOptional = portaRepository.findById(id);
        if(portaOptional.isPresent()){
            return portaOptional.get();
        } else {
            throw new PortaNotFoundException();
        }
    }

    public Porta criaPorta(InserePortaDTO inserePortaDTO) {

        Porta novaPorta = new Porta(
                inserePortaDTO.getSala(),
                inserePortaDTO.getAndar()
        );
        return portaRepository.save(novaPorta);
    }
}
