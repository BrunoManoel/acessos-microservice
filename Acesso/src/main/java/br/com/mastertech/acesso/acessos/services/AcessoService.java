package br.com.mastertech.acesso.acessos.services;

import br.com.mastertech.acesso.acessos.DTOs.AcessoDTO;
import br.com.mastertech.acesso.acessos.clients.ClienteClient;
import br.com.mastertech.acesso.acessos.clients.PortaClient;
import br.com.mastertech.acesso.acessos.clients.exceptions.ClienteNotFoundException;
import br.com.mastertech.acesso.acessos.clients.exceptions.PortaNotFoundException;
import br.com.mastertech.acesso.acessos.models.Acesso;
import br.com.mastertech.acesso.acessos.models.Cliente;
import br.com.mastertech.acesso.acessos.models.Porta;
import br.com.mastertech.acesso.acessos.repositories.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AcessoService {

    @Autowired
    ClienteClient clienteClient;

    @Autowired
    PortaClient portaClient;

    @Autowired
    AcessoRepository acessoRepository;

    public AcessoDTO cadastrarAcesso(AcessoDTO acessoDTO) {

        verificaIntegridadeDeAcesso(acessoDTO.getClienteId(), acessoDTO.getPortaId());

        Acesso novoAcesso = new Acesso(
                acessoDTO.getPortaId(),
                acessoDTO.getClienteId()
        );
        acessoRepository.save(novoAcesso);

        return acessoDTO;
    }

    private void verificaIntegridadeDeAcesso(int clienteId, int portaId){
        Optional<Porta> portaOptional = portaClient.getPorta(portaId);

        if(portaOptional.isPresent()){

            Optional<Cliente> clienteOptional = clienteClient.getCliente(clienteId);

            if(clienteOptional.isPresent()){

                Optional<Acesso> acessoOptional = acessoRepository.findByPortaIdAndClienteId(portaId, clienteId);

                if(acessoOptional.isPresent()) {
                    throw new RuntimeException("Acesso já concedido para este cliente nesta porta");
                }

            } else {
                throw new ClienteNotFoundException();
            }
        } else {
            throw new PortaNotFoundException();
        }
    }

}
