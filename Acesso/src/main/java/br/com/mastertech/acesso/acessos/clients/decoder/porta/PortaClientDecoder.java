package br.com.mastertech.acesso.acessos.clients.decoder.porta;

import br.com.mastertech.acesso.acessos.clients.exceptions.ClienteNotFoundException;
import br.com.mastertech.acesso.acessos.clients.exceptions.PortaNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class PortaClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404) {
            return new PortaNotFoundException();
        }
        return errorDecoder.decode(s, response);
    }

}
