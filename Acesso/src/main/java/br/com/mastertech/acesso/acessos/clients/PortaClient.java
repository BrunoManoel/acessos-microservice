package br.com.mastertech.acesso.acessos.clients;

import br.com.mastertech.acesso.acessos.clients.decoder.cliente.ClienteClientConfiguration;
import br.com.mastertech.acesso.acessos.clients.decoder.porta.PortaClientConfiguration;
import br.com.mastertech.acesso.acessos.models.Cliente;
import br.com.mastertech.acesso.acessos.models.Porta;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@FeignClient(name = "porta", configuration = PortaClientConfiguration.class)
public interface PortaClient {
    @GetMapping("/porta/{id}")
    Optional<Porta> getPorta(@PathVariable int id);
}
