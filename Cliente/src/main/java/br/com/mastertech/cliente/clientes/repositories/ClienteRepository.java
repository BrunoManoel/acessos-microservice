package br.com.mastertech.cliente.clientes.repositories;

import br.com.mastertech.cliente.clientes.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
}
