package br.com.mastertech.cliente.clientes.controllers;

import br.com.mastertech.cliente.clientes.DTOs.InsereClienteDTO;
import br.com.mastertech.cliente.clientes.models.Cliente;
import br.com.mastertech.cliente.clientes.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente registrarCliente(@RequestBody @Valid InsereClienteDTO clienteDTO) {
        Cliente clienteDB = clienteService.cadastrarCliente(clienteDTO);
        Cliente clienteResponse = new Cliente(clienteDB.getId(), clienteDB.getNome());

        return clienteResponse;
    }

    @GetMapping("/{id}")
    public Cliente findClientById(@PathVariable(name = "id") int id){
        return clienteService.findClientById(id);
    }


}
